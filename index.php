<?php

define('APPLICATION_PATH', dirname(__FILE__));
define("CONF_DIR", APPLICATION_PATH . "/conf");

$application = new \Yaf\Application( APPLICATION_PATH . "/conf/application.ini");

$application->bootstrap()->run();
?>
