<?php

/**
 * @name Bootstrap
 * @author Meow
 * @desc 所有在Bootstrap类中, 以_init开头的方法, 都会被Yaf调用,
 * @see http://www.php.net/manual/en/class.yaf-bootstrap-abstract.php
 * 这些方法, 都接受一个参数:\Yaf\Dispatcher $dispatcher
 * 调用的次序, 和申明的次序相同
 */
class Bootstrap extends \Yaf\Bootstrap_Abstract
{

    public function _initConfig() {
        //把配置保存起来
        $arrConfig = \Yaf\Application::app()->getConfig();
        \Yaf\Registry::set('config', $arrConfig);
    }

    public function _initPlugin(\Yaf\Dispatcher $dispatcher) {
        //注册一个插件
        $objSamplePlugin = new SamplePlugin();
        $dispatcher->registerPlugin($objSamplePlugin);
    }

    public function _initRoute(\Yaf\Dispatcher $dispatcher) {
        //在这里注册自己的路由协议,默认使用简单路由
    }

    public function _initView(\Yaf\Dispatcher $dispatcher) {
        //在这里注册自己的view控制器，例如smarty,firekylin
    }

    public function _initDatabase(\Yaf\Dispatcher $dispatcher) {
        Yaf\Loader::import(APPLICATION_PATH . '/application/library/vendor/autoload.php');
        $conf = new \Yaf\Config\Ini(CONF_DIR . '/database.ini', ini_get('yaf.environ'));
        $database = $conf->get('database')->toArray();
        // Eloquent ORM
        $capsule = new \Illuminate\Database\Capsule\Manager();
        $capsule->addConnection($database);          // 创建连接
        $capsule->setAsGlobal();                     // 设定全局静态访问
        $capsule->bootEloquent();                    // 启动Eloquet
    }

}
