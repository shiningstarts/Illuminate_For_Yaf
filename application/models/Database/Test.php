<?php

namespace Database;

use Illuminate\Database\Eloquent\Model;

/**
 * @desc 数据库测试model
 * Class TestModel
 * @package Database
 */
class TestModel extends Model
{
    protected $fillable = [
        'id',
    ];
    protected $table = 'test';

}