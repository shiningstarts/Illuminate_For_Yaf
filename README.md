# Illuminate For Yaf

## Usage

To run the example project, clone the repo
```
use Illuminate\Database\Eloquent\Model;

/**
 * @desc 数据库测试model
 * Class TestModel
 * @package Database
 */
class TestModel extends Model
{
}
```
```
\Database\TestModel::all();
```

## Requirements
PHP version >=7.0
Yaf extension for PHP

## Author

pingcat@foxmail.com

## License
Illuminate For Yaf is available under the MIT license. See the LICENSE file for more info.
